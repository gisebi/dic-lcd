/*
 * File: LcdTest.c
 *
 * Description: Tests the Lcd.c module
 *
 * Author : Sebastian Gie�auf
 */ 

#include <avr/io.h>
#include "Lcd.h"
#include "HtlStddef.h"
#include <string.h>
#include <stdio.h>

int MyPut(char aCharacter, FILE * aFile);

int main(void) {
    fdevopen(MyPut, NULL);
    
    LcdInit();
    
    DelayMs(500);
    
    unsigned char* write = "Hello LCD!";
    LcdSetCursor(1, 1);    
    // LcdWrite(write, strlen(write));
    printf("%s", write);
    
    while(1) {
        
    }
    
}

int MyPut(char aCharacter, FILE * aFile) {
    LcdWriteChar(aCharacter);
    return 1;
}