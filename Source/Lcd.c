/*
 * File: Lcd.c
 * 
 * Description: 
 *  Driver for an alpha-numeric LCD-Display. 
 *
 * Restrictions: 
 *  Microprocessor: ATMega644P
 *  Supports only the 4-bit interface
 * 
 * Author : Sebastian Gie�auf
 */ 

#include "Lcd.h"
#include "HtlStddef.h"
#include "LcdConstants.h"

/*
 * Declaration of public functions
 */

void LcdInit();
void LcdWrite(unsigned char * aBuffer, unsigned char aSize);
void LcdWriteChar(unsigned char aCharacter);
void LcdClear();
TBool LcdSetCursor(unsigned char aColumn, unsigned char aRow);

/*
 * Declaration of private functions
 */

static void LcdOut4Bit(unsigned char aByte);
static void LcdEnable();
static void LcdCommand(unsigned char aCommand);
static void LcdCheckBusy();
static TBool LcdReadBusyFlag();
static unsigned char LcdReadStatusRegister();
static unsigned char LcdRead4();

/*
 * Public functions
 */

/*
 * Function: LcdInit
 * 
 * Description:
 *  Initializes the LCD display
 */

void LcdInit() {
    LCD_PORT_DATA &= ~(0x0F << LCD_DB);
    LCD_DDR_DATA |= (0x0F << LCD_DB);
    
    LCD_PORT_RS &= ~(0x01 << LCD_RS);
    LCD_DDR_RS |= (0x01 << LCD_RS);
    
    LCD_PORT_RW &= ~(0x01 << LCD_RW);
    LCD_DDR_RW |= (0x01 << LCD_RW);
    
    LCD_PORT_EN &= ~(0x01 << LCD_EN);
    LCD_DDR_EN |= (0x01 << LCD_EN);
    
    LcdOut4Bit(LCD_SOFT_RESET);
    DelayMs(LCD_SOFT_RESET_MS1);
    LcdOut4Bit(LCD_SOFT_RESET);
    DelayMs(LCD_SOFT_RESET_MS2);
    LcdOut4Bit(LCD_SOFT_RESET);
    DelayMs(LCD_SOFT_RESET_MS3);
    
    // Set 4 bit Mode
    LcdOut4Bit(LCD_SET_FUNCTION |LCD_FUNCTION_4BIT);
    
    DelayMs(LCD_SET_4BITMODE_MS);
    
    // Initialize 2 lines, 5x7 Matrix
    LcdCommand(LCD_SET_FUNCTION | LCD_FUNCTION_4BIT | LCD_FUNCTION_2LINE | LCD_FUNCTION_5X7);
    
    // Initialize the cursor
    LcdCommand(LCD_SET_DISPLAY | LCD_DISPLAY_ON | LCD_CURSOR_OFF | LCD_BLINKING_OFF);
    
    // Cursor increment, no scroll
    LcdCommand(LCD_SET_ENTRY | LCD_ENTRY_INCREASE | LCD_ENTRY_NOSHIFT);
    
    LcdClear();
}

/*
 * Function: LcdWrite
 * 
 * Description:
 *  Writes the content of the char array to the current cursor position
 *
 * Parameters:
 *  aBuffer - The char array to write to the display
 *  aSize   - The length of the char array
 */

void LcdWrite(unsigned char * aBuffer, unsigned char aSize){
    for(int i = 0; i < aSize; i ++) {
        LcdWriteChar(aBuffer[i]);
    }
}

/*
 * Function: LcdWriteChar
 * 
 * Description:
 *  Writes the character to the current cursor position
 *
 * Parameters:
 *  aCharacter - The character to write
 */

void LcdWriteChar(unsigned char aCharacter) {
    LcdCheckBusy();
    LCD_PORT_RS |= (1 << LCD_RS);
    LCD_PORT_RW &= ~(1 << LCD_RW);
    LcdOut4Bit(aCharacter);
    LcdOut4Bit(aCharacter << 4);
}

/*
 * Function: LcdClear
 *
 * Description:
 *  Clears the Display
 */

void LcdClear(){
    LcdCommand(LCD_CLEAR_DISPLAY);
    DelayMs(LCD_CLEAR_DISPLAY_MS);
}

/*
 * Function: LcdSetCursor
 * Description:
 *  Set the position of the cursor to the specified values.
 *
 * Parameters:
 *  aColumn - The column starting with 1
 *  aRow    - The line of the display starting with 1
 * 
 * Returns:
 *  True if the position can be set, otherwise false
 */

TBool LcdSetCursor(unsigned char aColumn, unsigned char aRow){
    unsigned char temp;
    switch (aRow) {
        case 1:
            temp = LCD_DDADR_LINE1;
            break;
        case 2:
            temp = LCD_DDADR_LINE2;
            break;
        case 3:
            temp = LCD_DDADR_LINE3;
            break;
        case 4:
            temp = LCD_DDADR_LINE4;
            break;
        default:
            return EFALSE;
    }
    temp += aColumn - 1;
    LcdCommand(temp | LCD_SET_DDADR);
    return ETRUE;
}

/*
 * Private functions
 */

/*
 * Function: LcdOut4Bit
 * Description:
 *  Writes the upper nibble to the data lines. One enable impulse triggers the transfer.
 *
 * Parameters:
 *  aByte - The upper 4 bits are written to the data lines.
 */

static void LcdOut4Bit(unsigned char aByte) {
    LCD_DDR_DATA |= (0x0F << LCD_DB);
    LCD_PORT_RW &= ~(1 << LCD_RW);
    LCD_PORT_DATA &= ~(0x0F << LCD_DB);
    LCD_PORT_DATA |= ((aByte & 0xF0) >> (4 - LCD_DB));
    LcdEnable();
}

/*
 * Function: LcdEnable
 *
 * Description:
 *  Enables the LCD
 */
static void LcdEnable() {
    LCD_PORT_EN |= (0x01 << LCD_EN);
    DelayUs(LCD_ENABLE_US);
    LCD_PORT_EN &= ~(0x01 << LCD_EN);
    DelayUs(LCD_ENABLE_US);
}

/*
 * Function: LcdCommand
 *
 * Description:
 *  Sends the command to the LCD
 *
 * Parameters:
 *  aCommand - The command to execute
 */

static void LcdCommand(unsigned char aCommand) {
    LcdCheckBusy();
    
    LCD_PORT_RS &= ~(1 << LCD_RS);
    LcdOut4Bit(aCommand);
    LcdOut4Bit(aCommand << 4);
    
    DelayUs(LCD_COMMAND_US);
}

/*
 * Function: LcdCheckBusy
 *
 * Description:
 *  Delays the execution until the LCD is ready
 */

static void LcdCheckBusy() {
    unsigned int busyCounter = 0;
    
    while(LcdReadBusyFlag()){
        busyCounter++;
        if (busyCounter > LCD_MAX_BUSY_COUNTER) break;
        DelayUs(1);
    }
}

/*
 * Function: LcdReadBusyFlag
 *
 * Description:
 *  Returns the busy flag
 */

static TBool LcdReadBusyFlag() {
    unsigned char statusRegister;
    
    statusRegister = LcdReadStatusRegister();
    
    return ((statusRegister & 0x80) != 0);
}

/*
 * Function: LcdReadStatusRegister
 *
 * Description:
 *  Returns the status register of the LCD
 */

static unsigned char LcdReadStatusRegister() {
    unsigned char statusRegister;
    
    LCD_PORT_RS &= ~(1 << LCD_RS);
    statusRegister = LcdRead4();
    statusRegister = statusRegister << 4;
    statusRegister |= (LcdRead4() & 0x0F);
    
    return statusRegister;
}

/*
 * Function: LcdRead4
 *
 * Description:
 *  Returns 4 bit stored in the lower nibble
 */

static unsigned char LcdRead4() {
    unsigned char tmp;
    
    LCD_DDR_DATA &= ~(0x0F << LCD_DB);
    
    LCD_PORT_RW |= (1 << LCD_RW);
    LCD_PORT_EN |= (1 << LCD_EN);
    DelayUs(LCD_ENABLE_US);
    tmp = LCD_PIN_DATA;
    LCD_PORT_EN &= ~(1 << LCD_EN);
    tmp = (tmp >> LCD_DB) & 0x0F;
    
    LCD_DDR_DATA |= (0x0F << LCD_DB);
    
    return tmp;
}