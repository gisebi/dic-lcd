/*
 * File: Lcd.h
 * 
 * Description: 
 *  Driver for an alpha-numeric LCD-Display. 
 *
 * Restrictions: 
 *  Microprocessor: ATMega644P
 *  Supports only the 4-bit interface
 * 
 * Author : Sebastian Gie�auf
 */ 

#ifndef LCD_H
#define LCD_H

#include "HtlStddef.h"
#include "LcdConstants.h"

/*
 * Declaration of public functions
 */

/*
 * Function: LcdInit
 * 
 * Description:
 *  Initializes the LCD display
 */

void LcdInit();

/*
 * Function: LcdWrite
 * 
 * Description:
 *  Writes the content of the char array to the current cursor position
 *
 * Parameters:
 *  aBuffer - The char array to write to the display
 *  aSize   - The length of the char array
 */

void LcdWrite(unsigned char * aBuffer, unsigned char aSize);

/*
 * Function: LcdWriteChar
 * 
 * Description:
 *  Writes the character to the current cursor position
 *
 * Parameters:
 *  aCharacter - The character to write
 */

void LcdWriteChar(unsigned char aCharacter);

/*
 * Function: LcdClear
 *
 * Description:
 *  Clears the Display
 */

void LcdClear();

/*
 * Function: LcdSetCursor
 * Description:
 *  Set the position of the cursor to the specified values.
 *
 * Parameters:
 *  aColumn - The column starting with 1
 *  aRow    - The line of the display starting with 1
 * 
 * Returns:
 *  True if the position can be set, otherwise false
 */

TBool LcdSetCursor(unsigned char aColumn, unsigned char aRow);

#endif